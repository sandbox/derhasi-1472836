<?php

/**
 * @file
 *  Views implementation for an ajax load field.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_field_ajaxview_views_data_alter(&$data) {
  $data['views']['view_ajax_render'] = array(
    'title' => t('View rendered via ajax'),
    'help' => t('Embed a view as field. This can cause slow performance, so enable some caching.'),
    'field' => array(
      'handler' => 'views_field_ajaxview_handler_field_view',
    ),
  );
}
