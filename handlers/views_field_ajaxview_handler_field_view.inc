<?php

class views_field_ajaxview_handler_field_view extends views_field_view_handler_field_view {

  /**
   * Additional link class for identifying different views ajax render field
   * instances.
   *
   * @var string
   */
  protected $additional_field_class = 'views-ajax-render';

  function option_definition() {
    $options = parent::option_definition();
    $options['trigger_link_text'] = array('default' => 'View');
    $options['trigger_parent_selector'] = array('default' => FALSE);
    $options['trigger_parent_add_class'] = array('default' => '');
    $options['target_selector'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // We need no query aggregation.
    $form['query_aggregation'] = array(
      '#type' => 'value',
      '#value' => FALSE,
    );

    $form['views_field_ajaxview'] = array(
      '#type' => 'fieldset',
      '#title' => t("Ajax render settings"),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['trigger_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->options['trigger_link_text'],
      '#fieldset' => 'views_field_ajaxview',
    );

    $form['trigger_parent_selector'] = array(
      '#type' => 'textfield',
      '#title' => t('Trigger parent selector'),
      '#default_value' => $this->options['trigger_parent_selector'],
      '#description' => t('Enter a valid (jquery) selector if you want that parent element to also trigger the ajax render.'),
      '#fieldset' => 'views_field_ajaxview',
    );

    $form['trigger_parent_add_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Trigger parent: additioal class'),
      '#default_value' => $this->options['trigger_parent_add_class'],
      '#description' => t('If "trigger parent selector" is specified, an additional class can be set on the element to mark it active. Choose wisely, as the class will be triggered on the whole document.'),
      '#fieldset' => 'views_field_ajaxview',
    );

    $form['target_selector'] = array(
      '#type' => 'textfield',
      '#title' => t('Target selector'),
      '#default_value' => $this->options['target_selector'],
      '#description' => t('Enter a valid (jquery) selector for the target area.'),
      '#fieldset' => 'views_field_ajaxview',
    );
  }

  function pre_render(&$values) {
    static $runs = 0;
    $runs++;

    parent::pre_render($values);

    $this->additional_field_class = 'views-ajax-render-' . $runs;

    // In pre_render we add ajax, so we only add it once.
    ctools_include('ajax');

    if ($this->options['trigger_parent_selector']) {
      $setting = array(
        $this->additional_field_class => array(
          'trigger_parent_selector' => $this->options['trigger_parent_selector'],
          'target_selector' => $this->options['target_selector'],
          'trigger_parent_add_class' => $this->options['trigger_parent_add_class'],
        ),
      );
      drupal_add_js(array('viewsAjaxRender' => $setting), 'setting');
    }

    drupal_add_js(drupal_get_path('module', 'views_field_ajaxview') . '/js/views_field_ajaxview.js', 'file');
  }

  function render($values) {

    $args = array();
    // Only perform this loop if there are actually arguments present.
    if (!empty($this->options['arguments'])) {
      // Create array of tokens.
      foreach ($this->split_tokens($this->options['arguments']) as $token) {
        $args[] = $this->get_token_value($token, $values, $this->view);
      }
    }

    // @TODO: token replacement for target_selector.
    $data = array(
      'view' => $this->options['view'],
      'display' => $this->options['display'],
      'args' => $args,
      'selector' => $this->options['target_selector'],
      'path' => $this->view->get_path(),
    );

    // Write the specific data to the session, so it is not visible in URL.
    $token = drupal_get_token(drupal_json_encode($data));
    $_SESSION['views_field_ajaxview_field'][$token] = $data;

    // @TODO: token replacement for indicator_link_text.
    $link_title = $this->options['trigger_link_text'];
    $output = ctools_ajax_text_button($link_title, "views_field_ajaxview/nojs/view/$token", t('Replace text with "view"'), 'views-ajax-render-link '. $this->additional_field_class);
    return $output;
  }

}
