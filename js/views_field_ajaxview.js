/**
 * @file
 *   Additional code to attach ajax behaviour on parent elements.
 */

(function ($) {
  Drupal.behaviors.views_field_ajaxview = {
    attach: function (context, settings) {

      var module_settings = Drupal.settings.viewsAjaxRender;

      if (module_settings !== undefined) {
        $.each(module_settings, function(field_class, specs) {

          $('a.' + field_class).once(field_class).each(function () {
            var parent = $(this).parents(specs.trigger_parent_selector);
            var element_settings = {};

            // We show no addtional progress icon.
            element_settings.progress = { 'type': '' };

            // We use the target url from the link.
            element_settings.url = $(this).attr('href');
            element_settings.event = 'click';

            var base = parent.attr('id');

            // Prevent child links from bubbling up.
            parent.find('a').bind('click', function(event) {
              event.stopPropagation();
            });

            // Provide an additional active class for enabling the preview for the button.
            if (specs.trigger_parent_add_class) {
              parent.bind('click', function(event) {
                $('.' + specs.trigger_parent_add_class).removeClass(specs.trigger_parent_add_class);
                $(this).addClass(specs.trigger_parent_add_class);
              });
            }

            Drupal.ajax[base] = new Drupal.ajax(base, parent, element_settings);
          });
        });
      }
    }
  }
})(jQuery);